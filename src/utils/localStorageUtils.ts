import { ItemData } from "../model/ItemData";

function saveTodos(todos: ItemData) {
  localStorage.setItem("todos_key", JSON.stringify(todos));
}

function readTodos(): ItemData[] {
  // console.log(JSON.parse(localStorage.getItem("todos_key") || "[]"))
  return JSON.parse(localStorage.getItem("todos_key") || "[]");
}

export { saveTodos, readTodos };
